
(defconst my-org-packages
  '(org
    smartparens)
  )


(defun my-org/post-init-smartparens ()

  (smartparens-mode t)

  (defun my-org/count-character-backwards (char &optional limit)
    (save-excursion
      (let ((counter 0))
        (while (re-search-backward char limit t)
          (incf counter))
        counter)))

  (defun my-org/skip-func-inline-math (&rest _)
    (let ((count))
      (setq count (my-org/count-character-backwards (regexp-quote "$")
                                                    (line-beginning-position)))
      (cl-oddp count)))

  ;; replacement for sp--org-skip-asterix in smartparens-org
  (defun my-org/skip-func-* (_ms mb me)
    (let ((pos (point))
          (bol (line-beginning-position))
          count)
      (setq count (my-org/count-character-backwards (regexp-quote "*")
                                                    (line-beginning-position)))
      (or (= count (- pos bol)) (my-org/skip-func-inline-math))))

  (sp-with-modes 'org-mode
    (sp-local-pair "$" "$")
    (sp-local-pair "=" "="
                   :unless '(sp-point-after-word-p)
                   :skip-match 'my-org/skip-func-inline-math
                   :post-handlers '(("[d1]" "SPC")))
    (sp-local-pair "/" "/"
                   :unless '(sp-point-after-word-p)
                   :skip-match 'my-org/skip-func-inline-math
                   :post-handlers '(("[d1]" "SPC")))
    (sp-local-pair "*" "*"
                   ;; simply copied from smartparens-org
                   :unless '(sp-point-after-word-p sp-point-at-bol-p)
                   ;; added by me
                   :skip-match 'my-org/skip-func-*
                   :post-handlers '(("[d1]" "SPC")))))


(defun my-org/post-init-org ()

  ;; (defun my-org/insert-after-call (func)
  ;;   (funcall func)
  ;;   (org-insert-heading)
  ;;   (if insert
  ;;       (evil-append nil)))

  ;; (defun my-org/insert-heading-above (&optional insert)
  ;;   (interactive)
  ;;   (my-org/insert-after-call 'beginning-of-line))

  ;; (defun my-org/insert-heading-below (&optional insert)
  ;;   (interactive)
  ;;   (my-org/insert-after-call 'end-of-line))

  ;; (defun my-org/insert-heading-after-current (&optional insert)
  ;;   (interactive)
  ;;   (end-of-line)
  ;;   (org-insert-heading-after-current)
  ;;   (if insert
  ;;       (evil-append nil)))

  ;; (defun my-org/empty-line-p ()
  ;;   (let ((line (buffer-substring-no-properties
  ;;                (line-beginning-position)
  ;;                (line-end-position))))
  ;;     (numberp (string-match-p "^\\s-*$" line))))

  (defun my-org/metaright()
    (interactive)
    (org-metaright)
    (if (org-in-item-p)
        (if (my-org/first-item-p)
            (org-ctrl-c-minus))))

  (defun my-org/first-item-p ()
    (save-excursion
      (condition-case nil
          (progn
            (org-previous-item)
            nil)
        (error 't))))

  (defun my-org/in-item-p ()
    (condition-case nil
        (progn
          (my-org/beginning-of-item)
          't)
      (error nil)))

  (defun my-org/to-latex-pdf ()
    (interactive)
    (save-buffer)
    (org-latex-export-to-pdf))

  (defun my-org/to-open-latex-pdf ()
    (interactive)
    (org-open-file (my-org/to-latex-pdf))
    (evil-evilified-state t))

  (defun my-org/insert-item-after-current ()
    (if (my-org/in-item-p)
        (progn
          (my-org/end-of-item)
          (org-insert-item))
       (progn
         (if (>= (current-column) (current-indentation))
             (evil-org-open-below 1)
           (org-indent-line))
         (insert "- ")
         (evil-normal-state))))

  (defun my-org/end-of-item ()
    ;; org-end-of-item sets point to the bol of the first line after
    ;; the current item, jump back one point (i.e. the eol of the last
    ;; line of the current item) in order to prevent the indentation level
    ;; NOTE: would be good to find a less hacky solution
    (org-end-of-item)
    (goto-char (- (point) 1)))


  (defun my-org/end-of-item-position ()
    (save-excursion
      ;; (org-end-of-item)
      (my-org/end-of-item)))

  (defun my-org/beginning-of-item ()
    (save-excursion
      (org-beginning-of-item)))

  (defun my-org/mark-region (start end)
    (goto-char start)
    (push-mark end)
    (setq mark-active 't))

  ;; interactive functions
  (defun my-org/open-item-after-current ()
    (interactive)
    (my-org/insert-item-after-current)
    (evil-append 1))

  (defun my-org/open-subitem-after-current ()
    (interactive)
    (my-org/insert-item-after-current)
    (org-indent-item)
    (if (my-org/first-item-p)
        (org-cycle-list-bullet))
    (evil-append 1))

  (defun my-org/insert-subitem ()
    (interactive)
    (my-org/insert-item)
    (org-indent-item)
    (if (my-org/first-item-p)
        (org-cycle-list-bullet)))

  (defun my-org/insert-item ()
    (interactive)
    (let ((start (point))
          (end (if (my-org/in-item-p)
                   (my-org/end-of-item-position)
                 (line-end-position))))
      (if (>= (current-column) (current-indentation))
          (org-return-indent))
      (my-org/mark-region (point) (+ end (- (point) start)))
      (org-toggle-item 't)
      (org-mark-element)
      (org-outdent-item)))

  (add-hook 'org-mode-hook
  	    (lambda ()
          (smartparens-mode)
  	      (setq org-src-preserve-indentation 't)
          (setq org-log-done 'time)
          (setq org-src-fontify-natively 't)
          (setq org-highlight-latex-and-related '(latex script entities))
          (setq org-latex-listings 'minted)
          (setq org-latex-packages-alist '(("" "minted")))
          ;; (setq company-math-allow-latex-symbols-in-faces t)
          (setq org-catch-invisible-edits 'error)
          (setq yas-indent-line 'fixed)
          (setq org-latex-caption-above nil)

          ;; ;; modified to allow in-word emphasis
          ;; (org-set-emph-re 'org-emphasis-regexp-components
          ;;                  '(" \t('\"{\*/[:alpha:]"
          ;;                    "-[:alpha:] \t.,:!?;'\"\*/)}\\["
          ;;                    "    \r\n,"
          ;;                    "."
          ;;                    1)
          ;;                  )

          (setq org-latex-pdf-process
                (list "pdflatex --synctex=1 -interaction=nonstopmode -shell-escape %f"))


          (plist-put org-format-latex-options :scale 2)

          (evil-define-key '(normal insert) org-mode-map
            (kbd "M-*") 'org-ctrl-c-star
            (kbd "M--") 'org-ctrl-c-minus
            (kbd "M-l") 'my-org/metaright)

          ;; works as expected
          ;; (spacemacs/declare-prefix-for-mode 'org-mode "mh" "item")

          (spacemacs/set-leader-keys-for-major-mode 'org-mode
            "i i" 'my-org/insert-item
            "i I" 'my-org/insert-subitem
            "i o" 'my-org/open-item-after-current
            "i O" 'my-org/open-subitem-after-current
            ;; "i i" 'org-indent-item
            ;; "i I" 'org-indent-item-tree
            ;; "i o" 'org-outdent-item
            ;; "i O" 'org-outdent-item-tree
            "e o" 'my-org/to-open-latex-pdf
            "e p" 'my-org/to-latex-pdf)


          (org-babel-do-load-languages 'org-babel-load-languages
                                       '((python . t)
                                         (shell . t)
                                         (emacs-lisp . t)
                                         (dot . t)))

          ;; ;; what is that for?
          ;; (add-hook 'org-babel-after-execute-hook
          ;;           (lambda ()
          ;;             (when org-inline-image-overlays
          ;;               (org-redisplay-inline-images))))

          (setq org-confirm-babel-evaluate
                (lambda (lang body)
                  (not (string= lang "dot"))))

          )
        )
  )


(defconst my-python-packages
  '((python :location built-in)))

(defun my-python/post-init-python ()

  (defvar my-python/command-history '())

  (defun my-python/init-buffer ()
    (if (= (buffer-size) 0)
        (insert (concat
                 "#! /usr/bin/env python\n"
                 "# -*- coding: utf-8 -*-\n\n"
                 "if __name__ == \"__main__\":\n"
                 "    pass"))))

  (defun my-python/occur ()
    (interactive)
    (occur "^\\s-*\\b\\(class\\|def\\)\\b"))


  (defun my-python/python-query-execute-file ()
    "Execute a python command in a shell. Query the user for input."
    (interactive)
    (let* ((default-compile-command
             (or (car my-python/command-history)
                 (format "%s %s"
                         (spacemacs/pyenv-executable-find python-shell-interpreter)
                         (shell-quote-argument (file-name-nondirectory buffer-file-name)))))
           (compile-command (read-string "command: " default-compile-command)))
      (push compile-command  my-python/command-history)
      (compile (car my-python/command-history) 't)
      (with-current-buffer (get-buffer "*compilation*")
        (inferior-python-mode))))

  (defun my-python/blacken-format-buffer ()
    (interactive)
    (let ((blacken-line-length 120))
    ;; (let ((blacken-line-length 60))
      (blacken-buffer)))

  (require 'pytest)

  (defun my-python/yas-breakpoint ()
    (interactive)
    (yas-expand-snippet (yas-lookup-snippet 'breakpoint)))

  (defun my-python/pytest-test-last ()
    (interactive)
    (let ((pytest-buffer (pytest-get-temp-buffer-name)))
      (if (get-buffer pytest-buffer)
        (with-current-buffer pytest-buffer
          (recompile))
        (error "pytest did not run yet"))))

  (add-hook 'python-mode-hook
            (lambda ()

              (my-python/init-buffer)

              ;; (spacemacs/set-leader-keys-for-major-mode 'python-mode "c n" 'next-error)
              ;; (spacemacs/set-leader-keys-for-major-mode 'python-mode "c p" 'previous-error)
              (spacemacs/set-leader-keys-for-major-mode 'python-mode "c k" 'kill-compilation)
              ;; (spacemacs/set-leader-keys-for-major-mode 'python-mode "c d" 'spacemacs/close-compilation-window)
              (spacemacs/set-leader-keys-for-major-mode 'python-mode "c q" 'my-python/python-query-execute-file)

              ;; (spacemacs/set-leader-keys-for-major-mode 'python-mode "t l" 'my-python/pytest-test-last)
              ;; (spacemacs/set-leader-keys-for-major-mode 'python-mode "t k" 'kill-compilation)

              (spacemacs/set-leader-keys-for-major-mode 'python-mode "= f" 'my-python/blacken-format-buffer)
              (spacemacs/set-leader-keys "iSs" 'yas-insert-snippet)
              (spacemacs/set-leader-keys "iSb" 'my-python/yas-breakpoint)

              (setq compilation-always-kill 't)
              ;; (setq python-test-runner 'pytest)
              ))
  )

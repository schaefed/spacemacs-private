;;; -*- lexical-binding: t -*-
;;; packages.el --- my-config layer packages file for Spacemacs.
;;
;; Copyright (c) 2012-2017 Sylvain Benner & Contributors
;;
;; Author:  <schaefed@power>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

;;; Commentary:

;; See the Spacemacs documentation and FAQs for instructions on how to implement
;; a new layer:
;;
;;   SPC h SPC layers RET
;;
;;
;; Briefly, each package to be installed or configured by this layer should be
;; added to `my-config-packages'. Then, for each package PACKAGE:
;;
;; - If PACKAGE is not referenced by any other Spacemacs layer, define a
;;   function `my-config/init-PACKAGE' to load and initialize the package.

;; - Otherwise, PACKAGE is already referenced by another Spacemacs layer, so
;;   define the functions `my-config/pre-init-PACKAGE' and/or
;;   `my-config/post-init-PACKAGE' to customize the package as it is loaded.

;;; Code:

(defconst my-config-packages
  '())


;; (defun my-repeatable/init-key-map (key-func-alist &optional key-map)
;;   (let ((map (or key-map (make-sparse-keymap))))
;;     (when key-func-alist
;;       (define-key map (kbd (caar key-func-alist)) (cdar key-func-alist))
;;       (my-repeatable/init-key-map (cdr key-func-alist) map)
;;       )
;;     map))

;; (defun my-repeatable/set-repeatable (key-func-alist &optional key-map)
;;   ;; Returns the function in the first assoc
;;   ;; passing a key-map does not seem to work...
;;   (let ((map (or key-map (my-repeatable/init-key-map key-func-alist))))
;;     (when key-func-alist
;;       (advice-add (cdar key-func-alist) :after
;;                   (lambda (&rest r) ;; gets the same arguments as the adviced function
;;                     (set-transient-map map 't)))
;;       (my-repeatable/set-repeatable (cdr key-func-alist) key-map))
;;     (cdar key-func-alist)))

;; (defun my-comment-or-uncomment-below()
;;   (interactive)
;;   (if (use-region-p)
;;       (progn
;;         (call-interactively 'comment-or-uncomment-region)
;;         (goto-char (region-end)))
;;     (call-interactively 'comment-line)))

(defun my-enlarge-window()
  (interactive)
  (enlarge-window 4))

(defun my-shrink-window()
  (interactive)
  (shrink-window 4))

(defun my-kill-buffer()
  (interactive)
  (kill-buffer (current-buffer)))

(defun my-toggle-maximize-buffer ()
  (interactive)
  (if (= 1 (length (window-list)))
      (jump-to-register '_)
    (progn
      (window-configuration-to-register '_)
      (delete-other-windows))))

(defun my-kill-star-buffer-windows ()
  (interactive)
  (mapc (lambda (window)
          (if (string-match-p "^\\*.*\\*$" (buffer-name (window-buffer window)))
              (delete-window window)))
        (window-list)))

;; (defun my-transpose-with-previous-line ()
;;   (interactive)
;;   (let ((col (current-column)))
;;     (when (> (line-number-at-pos) 1)
;;       (transpose-lines 1)
;;       (forward-line -2)
;;       (move-to-column col)
;;       )
;;     )
;;   )

;; (defun my-transpose-with-next-line ()
;;   (interactive)
;;   (let ((col (current-column)))
;;     ;; (when (< (line-number-at-pos) (line-number-at-pos (point-max)))
;;     (forward-line 1)
;;     (transpose-lines 1)
;;     (forward-line -1)
;;     (move-to-column col)
;;     ;; )
;;     )
;;   )

(defun my-config/y-or-no-p (prompt)
  (let ((answer (read-from-minibuffer (concat prompt "([y] or n) "))))
    (if (member answer '("y" "yes" ""))
        t
      nil)))

(defalias 'yes-or-no-p 'my-config/y-or-no-p)
(defalias 'y-or-n-p 'my-config/y-or-no-p)

(with-eval-after-load 'evil-maps
  (define-key evil-normal-state-map (kbd "C-<delete>")		'my-kill-buffer)

  ;; (define-key evil-normal-state-map (kbd "M-;")		'my-comment-or-uncomment-below)
  ;; (define-key evil-normal-state-map (kbd "M-k") 'my-transpose-with-previous-line)
  ;; (define-key evil-normal-state-map (kbd "M-j") 'my-transpose-with-next-line)

  (global-set-key (kbd "M-#") 'hippie-expand)

  (spacemacs/set-leader-keys "c ," 'comment-dwim)

  (spacemacs/set-leader-keys "a r" 'align-regexp)

  (spacemacs/set-leader-keys "b l" 'list-buffers)
  (spacemacs/set-leader-keys "b *" 'my-kill-star-buffer-windows)

  ; (spacemacs/set-leader-keys "b M" 'spacemacs/kill-other-buffers)
  (spacemacs/set-leader-keys "b M" 'my-toggle-maximize-buffer)

  ;; downcase word
  (define-key evil-normal-state-map (kbd "M-d") 'downcase-word)
  (define-key evil-insert-state-map (kbd "M-d") 'downcase-word)

  ;; resize buffer
  (define-key evil-normal-state-map (kbd "C-c +") 'my-enlarge-window)
  (define-key evil-normal-state-map (kbd "C-c -") 'my-shrink-window)



  ;; (my-repeatable/set-repeatable '(("o" . other-window)
  ;;                                 ("O" . previous-multiframe-window)))

  ;; (my-repeatable/set-repeatable '(("+" . my-enlarge-window)
  ;;                                 ("-" . my-shrink-window)))
)

;; (setq plantuml-jar-path "/usr/bin/plantuml")

;;; packages.el ends here
